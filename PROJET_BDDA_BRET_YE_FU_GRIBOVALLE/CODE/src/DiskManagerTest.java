import java.io.IOException;
import java.nio.ByteBuffer;

public class DiskManagerTest {
    public static void main(String[] args) throws IOException {
        DBParams.DBPath = "./DB/";
        DBParams.pageSize = 4096;
        DBParams.maxPagesPerFile = 4;

        // TEST DE LA CLASSE DISKMANAGER
        DiskManager dm = DiskManager.getSingleton();
        PageId pageId1 = dm.allocPage();
        // PageId pageId2 = dm.allocPage();
        // PageId pageId3 = dm.allocPage();
        // PageId pageId4 = dm.allocPage();
        // PageId pageId5 = dm.allocPage();
        // PageId pageId6 = dm.allocPage();
        // PageId pageId7 = dm.allocPage();
        // PageId pageId8 = dm.allocPage();
        // PageId pageId9 = dm.allocPage();
        // PageId pageId10 = dm.allocPage();

        ByteBuffer bf = ByteBuffer.allocate(DBParams.pageSize);
        bf.putChar('t');
        dm.writePage(pageId1, bf);

        ByteBuffer bf2 = ByteBuffer.allocate(DBParams.pageSize);
        dm.readPage(pageId1, bf2);

        bf.position(0);

        // System.out.println(bf2.getChar());
        // System.out.println(bf.getChar());
        assert (bf2.getChar() == bf.getChar());

        // dm.writePage(pageId10, bf);
        // dm.readPage(pageId10, bf2);
        // assert(bf2.getChar() == bf.getChar());
    }
}