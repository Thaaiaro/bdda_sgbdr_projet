import java.util.ArrayList;
import java.util.List;

/**
 * Classe permettant la gestion des pages de notre base de données
 * 
 * @author GRIBOVALLE Mathieu, BRET David, YE Frederic, FU SuTanqQin
 * @version 1.0
 * @date 2022-11-24
 */
public class FileManager {
    /**
     * Crée l'unique instance du FileManager
     */
    private static FileManager g_instance = new FileManager();

    /**
     * Création d'une nouvelle HeaderPage et de son contenu
     * 
     * @return le PageId de la HeaderPage créée
     */
    public PageId createNewHeaderPage() {
        DiskManager dm = DiskManager.getSingleton();
        PageId pid = null;

        pid = dm.allocPage();
        HeaderPage hpage = new HeaderPage(pid); // get(hpage)
        hpage.setDataPageCount(0);
        hpage.free(); // free(hpage)

        return pid;
    }

    /**
     * Ajoute une DataPage pour la relation relInfo
     * 
     * @param relInfo la relation
     * @return le PageId de la page ajoutée
     */
    public PageId addDataPage(RelationInfo relInfo) {
        DiskManager dm = DiskManager.getSingleton();
        PageId pid = null;

        HeaderPage hpage = new HeaderPage(relInfo.getHeaderPageId()); // get(hpage)

        pid = dm.allocPage();
        DataPage dpage = new DataPage(pid, relInfo); // get(dpage)
        dpage.setInitPosition(0);
        dpage.setRecordCount(0);
        hpage.setDataPageCount(hpage.getDataPageCount() + 1);

        hpage.free(); // free(hpage)
        dpage.free(); // free(dpage)

        return pid;
    }

    /**
     * Désigne une page sur laquelle il reste assez de place pour insérer le Record
     * 
     * @param relInfo    la relation
     * @param sizeRecord taille du record à insérer
     * @return le PageId d'une DataPage sur laquelle il reste assez de place
     */
    public PageId getFreeDataPageId(RelationInfo relInfo, int sizeRecord) {
        HeaderPage hpage;

        hpage = new HeaderPage(relInfo.getHeaderPageId()); // get(hpage)
        for (int i = 0; i < hpage.getDataPageCount(); i++) {
            if (hpage.getFreeSpace(i) >= sizeRecord) {
                PageId pid = hpage.getDataPageId(i);
                return pid;
            }
        }

        hpage.free(); // free(hpage)
        return null;
    }

    /**
     * Ecrit un record dans la DataPage de PageId pid
     * 
     * @param record le record à écrire
     * @param pid    le PageId de la DataPage
     * @return Le RecordId du Record écrit
     */
    public RecordId writeRecordToDataPage(Record record, PageId pid) {
        DataPage dpage = new DataPage(pid, record.getRelInfo());
        int initPosition = dpage.getInitPosition();

        HeaderPage hpage = new HeaderPage(record.getRelInfo().getHeaderPageId());

        record.writeToBuffer(dpage.getBuffer(), dpage.getInitPosition());

        RecordId res = new RecordId(pid, dpage.getRecordCount() + 1);

        dpage.setInitPosition(initPosition + record.getSize());
        dpage.setRecordCount(dpage.getRecordCount() + 1);
        dpage.getBuffer().position(DBParams.pageSize - 2 * Integer.BYTES - dpage.getRecordCount() * 2 * Integer.BYTES);
        dpage.getBuffer().putInt(initPosition);
        dpage.getBuffer().putInt(record.getSize());

        hpage.setFreeSpace(hpage.getDataPageIdIndex(pid),
                hpage.getFreeSpace(hpage.getDataPageIdIndex(pid)) - record.getSize());

        dpage.free();
        hpage.free();
        return res;
    }

    /**
     * Renvoie la liste des records stockés dans la DataPage de PageId pid
     * 
     * @param relInfo la relation
     * @param pid     le PageId de la page
     * @return la liste de Record dans la DataPage
     */
    List<Record> getRecordsInDataPage(RelationInfo relInfo, PageId pid) {
        List<Record> recordsList = new ArrayList<>();

        DataPage dpage = new DataPage(pid, relInfo); // get(dpage)
        for (int i = 0; i < dpage.getRecordCount(); i++) {
            Record record = new Record(relInfo);
            record.readFromBuffer(dpage.getBuffer(), i * Integer.BYTES);
            recordsList.add(record);
        }
        dpage.free(); // free(dpage)

        return recordsList;
    }

    /**
     * Donne la liste de tous les PageId de toutes les DataPage de la relation
     * 
     * @param relInfo la relation
     * @return la liste des PageId de toutes les DataPage de la relation
     */
    public List<PageId> getAllDataPages(RelationInfo relInfo) {
        List<PageId> pidList = new ArrayList<>();
        HeaderPage hpage = new HeaderPage(relInfo.getHeaderPageId());

        for (int i = 0; i < hpage.getDataPageCount(); i++) {
            pidList.add(hpage.getDataPageId(i));
        }
        hpage.free();
        return pidList;
    }

    /**
     * Insère un Record dans la relation
     * 
     * @param rec le Record à insérer
     * @return retourne le RecordId du record inséré
     */
    public RecordId insertRecordIntoRelation(Record rec) {
        return null;
    }

    /**
     * Donne la liste de tous les Record de la relation relInfo
     * 
     * @param relInfo la relation
     * @return la liste de tous les Record de la relation
     */
    // public List<Record> getAllRecords(RelationInfo relInfo) {

    // }

    /**
     * Création de l'instance unique du FileManager
     * 
     * @return l'instance de FileManager
     */
    public static FileManager getSingleton() {
        return g_instance;
    }
}