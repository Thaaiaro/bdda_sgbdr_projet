import java.io.Serializable;

/**
 * Classe permettant d'accéder aux pages par un identifiant
 * 
 * @author GRIBOVALLE Mathieu, BRET David, YE Frederic, FU SuTanqQin
 * @version 1.0
 * @date 2022-09-22
 */
public class PageId implements Serializable {

    /**
     * Le fichier dans lequel se situe la page
     */
    private int fileIdx;

    /**
     * Le numéro de la page dans le fichier
     */
    private int pageIdx;

    /**
     * Constructeur d'accès à une page pour un identifiant (f,p)
     * 
     * @param f le fichier dans lequel est situé la page
     * @param p le numéro de la page dans le fichier
     */
    public PageId(int f, int p) {
        fileIdx = f;
        pageIdx = p;
    }

    /**
     * Evite les doublons de pages dans une HashMap
     */
    @Override
    public int hashCode() {
        return ((Integer) fileIdx).hashCode() + ((Integer) pageIdx).hashCode();
    }

    /**
     * Getter du fichier dans lequel est situé la page
     * 
     * @return le fichier dans lequel est situé la page
     */
    public int getFileIdx() {
        return fileIdx;
    }

    /**
     * Getter du numéro de la page dans le fichier
     * 
     * @return le numéro de la page dans le fichier
     */
    public int getPageIdx() {
        return pageIdx;
    }

    /**
     * Affichage du PageId sous forme d'une chaine de caractères
     * 
     * @return le PageId sous forme de String
     */
    @Override
    public String toString() {
        StringBuilder build = new StringBuilder();

        build.append("PageId(" + fileIdx + ", " + pageIdx + ")");

        return build.toString();
    }

}