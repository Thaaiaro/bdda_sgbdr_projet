import java.nio.ByteBuffer;

/**
 * Classe permettant de structurer et d'accéder aux éléments de notre HeaderPage
 * 
 * @author GRIBOVALLE Mathieu, BRET David, YE Frederic, FU SuTanqQin
 * @version 1.0
 * @date 2022-11-17
 */
public class DataPage {

    /**
     * Le buffer de la page
     */
    private ByteBuffer mBuffer;

    /**
     * l'identifiant de la DataPage
     */
    private PageId mPageId;

    /**
     * Le FlagDirty de la DataPage
     */
    private boolean mDirty;

    /**
     * La relation dont la DataPage contient des Record
     */
    private RelationInfo mRelInfo;

    /**
     * Constructeur qui permet d'accéder à une DataPage
     * 
     * @param pid     le pid de la DataPage
     * @param relInfo les informations de la relation
     */
    public DataPage(PageId pid, RelationInfo relInfo) {
        mRelInfo = relInfo;
        mPageId = pid;
        mBuffer = BufferManagerCopy.getSingleton().getPage(pid);
    }

    /**
     * Compte le nombre de Record sur la page
     * 
     * @return le nombre de record
     */
    public int getRecordCount() {
        mBuffer.position(DBParams.pageSize - 2 * Integer.BYTES);

        return mBuffer.getInt();
    }

    /**
     * Permet d'actualiser le nombre de Record présent sur la page
     * 
     * @param count nouveau nombre de record
     */
    public void setRecordCount(int count) {
        mDirty = true;

        mBuffer.position(DBParams.pageSize - 2 * Integer.BYTES);
        mBuffer.putInt(count);
    }

    /**
     * Donne la position de début d'espace disponible
     * 
     * @return la position à partir de laquelle l'espace est libre dans la page
     */
    public int getInitPosition() {
        mBuffer.position(DBParams.pageSize - Integer.BYTES);

        return mBuffer.getInt();
    }

    /**
     * Permet d'actualiser la position de début d'espace disponible
     * 
     * @param count nouvelle position de début d'espace disponible
     */
    public void setInitPosition(int count) {
        mDirty = true;

        mBuffer.position(DBParams.pageSize - Integer.BYTES);
        mBuffer.putInt(count);
    }

    /**
     * Retourne la quantité d'espace libre sur la page
     * 
     * @return le nombre d'octet libre sur la page
     */
    public int getFreeSpace() {
        return DBParams.pageSize - 2 * Integer.BYTES - getInitPosition();
    }

    /**
     * Retourne le buffer de la page
     * 
     * @return le buffer de la page
     */
    public ByteBuffer getBuffer() {
        return mBuffer;
    }

    /**
     * Actualise la taille d'un record
     * 
     * @param i    l'index du record dont on veut changer la taille
     * @param size la taille du record
     */
    public void setSizeRecord(int i, int size) {
        mDirty = true;

        mBuffer.position(DBParams.pageSize - 2 * Integer.BYTES - i * 2 * Integer.BYTES + Integer.BYTES);
        mBuffer.putInt(size);
    }

    /**
     * Retourne la taille d'un record
     * 
     * @param i l'index du record dont on veut connaitre la taille
     * @return la taille du record
     */
    public int getSizeRecord(int i) {
        mBuffer.position(DBParams.pageSize - 2 * Integer.BYTES - i * 2 * Integer.BYTES + Integer.BYTES);

        return mBuffer.getInt();
    }

    /**
     * Retourne la position de début d'un record
     * 
     * @param i l'index du record dont on veut connaitre la position
     * @return la position de début du record
     */
    public int getInitPosRecord(int i) {
        return DBParams.pageSize - Integer.BYTES - i * 2 * Integer.BYTES;
    }

    /**
     * Permet d'actualiser la position de début d'un record
     * 
     * @param i    l'index du record dont on veut actualiser la position de départ
     * @param size la taille du record
     */
    public void setInitPosRecord(int i, int size) {
        mDirty = true;

        mBuffer.position(DBParams.pageSize - Integer.BYTES - i * 2 * Integer.BYTES);
        mBuffer.putInt(size * getRecordCount());
    }

    /**
     * Libère la DataPage
     */
    public void free() {
        BufferManagerCopy.getSingleton().freePage(mPageId, mDirty);
    }

    /**
     * Permet l'affichage de la DataPage
     * 
     * @return la DataPage structurée sous la forme d'une chaine de caractère
     */
    @Override
    public String toString() {
        StringBuilder build = new StringBuilder();

        for (int i = 0; i < getRecordCount(); i++) {
            Record rec = new Record(mRelInfo);
            rec.readFromBuffer(mBuffer, getInitPosRecord(i));
            build.append("Record " + i + ": " + rec.values + "\n");
        }
        for (int i = getRecordCount(); i > 0; i++) {
            build.append("Position début Record " + i + " : "
                    + (DBParams.pageSize - 2 * Integer.BYTES - i * 2 * Integer.BYTES));
            build.append("/ Taille Record " + i + " : " + getSizeRecord(i) + "\n");

        }
        build.append("Nombre de Record: " + getRecordCount() + "\n");
        build.append("Position début espace libre: " + getInitPosition() + "\n");

        return build.toString();
    }
}
