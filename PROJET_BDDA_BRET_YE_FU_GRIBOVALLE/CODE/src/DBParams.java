/**
 * Paramètres de la base de données
 * 
 * @author GRIBOVALLE Mathieu, BRET David, YE Frederic, FU SuTanqQin
 * @version 1.1
 * @date 2022-09-22
 */
public class DBParams {

    /**
     * Chemin vers la Base de Données
     */
    public static String DBPath = "./DB/";

    /**
     * Taille d'une page
     */
    public static int pageSize = 4096;

    /**
     * Nombre de pages maximum par fichier
     */
    public static int maxPagesPerFile = 4;

    /**
     * Nombre de frame dans le BufferManager
     */
    public static int frameCount = 2;
}
