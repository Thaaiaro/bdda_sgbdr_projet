import java.nio.ByteBuffer;
import java.util.LinkedList;

//ATTENTION A AVOIR AUTANT DE FREEPAGE QUE DE GETPAGE

/**
 * Classe de gestion des différentes frames de notre BufferManager
 * 
 * @author GRIBOVALLE Mathieu, BRET David, YE Frederic, FU SuTanqQin
 * @version 1.0
 * @date 2022-09-22
 */
public class BufferManagerCopy {

    /**
     * L'unique instance de BufferManager
     */
    private static BufferManagerCopy g_instance = new BufferManagerCopy();

    /**
     * Liste de frames que le BufferManager peut utiliser
     */
    public Frame[] listeFrames = new Frame[DBParams.frameCount];

    /**
     * Liste chaînée de frame qui donne les frames disponibles
     */
    private LinkedList<Frame> frameDispo = new LinkedList<>();

    /**
     * Permet d'initialiser notre BufferManager avec le nombre prédéfini de frame
     */
    public BufferManagerCopy() {
        for (int i = 0; i < listeFrames.length; i++) {
            listeFrames[i] = new Frame();
        }
    }

    /**
     * Permet de charger une page (déjà chargée ou non en mémoire vive) dans une
     * frame du BufferManager
     * Gère la politique de remplacement LRU, et les écritures sur le disque
     * 
     * @param pid PageId de la page chargée
     * @return le buffer de la frame chargée
     */
    public ByteBuffer getPage(PageId pid) {

        // Get la page de PageId pid si déjà dans une frame
        for (int i = 0; i < listeFrames.length; i++) {
            if (listeFrames[i].getPid() == pid) {
                listeFrames[i].setPinCount(listeFrames[i].getPinCount() + 1);
                return listeFrames[i].getBuff();
            }
        }

        // Get la page de PageId pid si une frame est vide
        for (int i = 0; i < listeFrames.length; i++) {
            if (listeFrames[i].getPid() == null) {

                DiskManager.getSingleton().readPage(pid, listeFrames[i].getBuff());
                listeFrames[i].setPid(pid);
                listeFrames[i].setPinCount(listeFrames[i].getPinCount() + 1);
                return listeFrames[i].getBuff();
            }
        }

        // Si toutes les frames sont pleines, on utilise la politique de remplacement
        // LRU.
        boolean frameTrouve = false;
        for (int i = 0; i < listeFrames.length && !frameTrouve; i++) {
            if (listeFrames[i].getPinCount() == 0) {
                frameTrouve = true;
            }
        }
        if (!frameTrouve) {
            System.err.println("Le BufferManager ne trouve aucune frame libre");
        } else {
            // Ecriture du contenu de la frame
            if (frameDispo.getFirst().getFlagDirty() == true) {
                DiskManager.getSingleton().writePage(frameDispo.get(0).getPid(), frameDispo.get(0).getBuff());
                frameDispo.getFirst().setFlagDirty(false);
            }

            // Remplacement de la frame par la frame de pid donné
            for (int i = 0; i < listeFrames.length; i++) {
                if (frameDispo.getFirst().getPid() == listeFrames[i].getPid()) {
                    // Lecture du contenu de la frame
                    frameDispo.remove(0);
                    DiskManager.getSingleton().readPage(pid, listeFrames[i].getBuff());
                    listeFrames[i].setPid(pid);
                    listeFrames[i].setPinCount(listeFrames[i].getPinCount() + 1);
                    return listeFrames[i].getBuff();
                }
            }
        }

        System.err.println("Plus de page disponible");
        return null;
    }

    /**
     * Libère une page chargée dans une frame
     * 
     * @param pid      PageId de la page à libérer
     * @param valDirty Valeur du FlagDirty de la Frame
     */
    public void freePage(PageId pid, boolean valDirty) {
        for (int i = 0; i < listeFrames.length; i++) {
            if (listeFrames[i].getPid() == pid) {

                if (listeFrames[i].getPinCount() >= 1)
                    listeFrames[i].setPinCount(listeFrames[i].getPinCount() - 1);

                if (listeFrames[i].getFlagDirty() == false && valDirty == true)
                    listeFrames[i].setFlagDirty(true);

                if (listeFrames[i].getPinCount() == 0) {
                    frameDispo.add(listeFrames[i]);
                }
                // System.out.println(frameDispo.get(0));
                // System.out.println(frameDispo.get(0).getFlagDirty());
            }
        }
    }

    /**
     * Ecrit toutes les pages sur le disque et remet le BufferManager à 0
     */
    public void flushAll() {
        for (int i = 0; i < listeFrames.length; i++) {
            if (listeFrames[i].getFlagDirty() == true) {
                DiskManager.getSingleton().writePage(listeFrames[i].getPid(), listeFrames[i].getBuff());
            }
            listeFrames[i].setFlagDirty(false);
            listeFrames[i].setPinCount(0);
            listeFrames[i].setPid(null);
            listeFrames[i].getBuff().clear();
        }
    }

    /**
     * Création de l'unique instance du BufferManager
     * 
     * @return l'instance du BufferManager
     */
    public static BufferManagerCopy getSingleton() {
        return g_instance;
    }
}