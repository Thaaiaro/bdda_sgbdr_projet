public class FileManagerTest {

    public static void main(String[] args) {
        FileManager fm = FileManager.getSingleton();

        PageId pid = fm.createNewHeaderPage();
        HeaderPage hpage = new HeaderPage(pid);

        PageId id2 = DiskManager.getSingleton().allocPage();
        PageId id3 = DiskManager.getSingleton().allocPage();
        PageId id4 = DiskManager.getSingleton().allocPage();
        PageId id5 = DiskManager.getSingleton().allocPage();
        hpage.addDataPage(id2);
        hpage.addDataPage(id3);
        hpage.addDataPage(id4);
        hpage.addDataPage(id5);

        System.out.println("ID HeaderPage = " + hpage.getPid());
        System.out.println(hpage);

        ColInfo colonne1 = new ColInfo("Nom", "CHARACTER");
        ColInfo[] colonnes = new ColInfo[1];
        colonnes[0] = colonne1;

        RelationInfo relInfo = new RelationInfo("Etudiant", 1, colonnes, pid);
        Record rec1 = new Record(relInfo);

        DataPage dpage = new DataPage(id2, relInfo);

        fm.writeRecordToDataPage(rec1, id2);

        System.out.println(hpage);
        System.out.println(dpage);
    }
}
