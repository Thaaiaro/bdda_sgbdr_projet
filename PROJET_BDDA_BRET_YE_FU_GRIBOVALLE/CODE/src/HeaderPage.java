import java.nio.ByteBuffer;

/**
 * Classe permettant de structurer et d'accéder aux éléments de notre HeaderPage
 * 
 * @author GRIBOVALLE Mathieu, BRET David, YE Frederic, FU SuTanqQin
 * @version 1.0
 * @date 2022-11-17
 */
public class HeaderPage {
    /**
     * Buffer de la HeaderPage
     */
    private ByteBuffer mBuffer;

    /**
     * PageId de la HeaderPage
     */
    private PageId mPageId;

    /**
     * FlagDirty de la HeaderPage, passe à true si le contenu de la HeaderPage est
     * modifiéé
     */
    private boolean mDirty;

    /**
     * Constructeur de HeaderPage, Accède à la page d'identifiant pid et la
     * transforme en HeaderPage
     * 
     * @param pid le PageId de l'HeaderPage
     */
    public HeaderPage(PageId pid) {
        mPageId = pid;
        mBuffer = BufferManagerCopy.getSingleton().getPage(pid);
    }

    /**
     * Modifie le nombre de DataPage inscrit dans la HeaderPage
     * 
     * @param count nouvelle valeur de nombre de DataPage
     */
    public void setDataPageCount(int count) {
        mDirty = true;

        mBuffer.position(0);
        mBuffer.putInt(count);
    }

    /**
     * Ajoute une DataPage de PageId pid
     * 
     * @param pid le pid de la DataPage à ajouter
     */
    public void addDataPage(PageId pid) {
        mDirty = true;
        int dataPageCount = getDataPageCount();

        mBuffer.position(Integer.BYTES + dataPageCount * Integer.BYTES * 3);
        mBuffer.putInt(pid.getFileIdx());
        mBuffer.putInt(pid.getPageIdx());
        mBuffer.putInt(DBParams.pageSize - 2 * Integer.BYTES);

        setDataPageCount(getDataPageCount() + 1);
    }

    /**
     * Getter du nombre de DataPage inscrit dans le HeaderPage
     * 
     * @return le nombre de DataPage
     */
    public int getDataPageCount() {
        mBuffer.position(0);

        return mBuffer.getInt();
    }

    /**
     * Getter du PageId de la page d'indice id
     * 
     * @param id l'indice de la page dont on veut le PageId
     * @return le PageId de la page d'indice id
     */
    public PageId getDataPageId(int id) {
        mBuffer.position(Integer.BYTES + id * Integer.BYTES * 3);

        int fileIdx = mBuffer.getInt();
        int pageIdx = mBuffer.getInt();

        return new PageId(fileIdx, pageIdx);
    }

    /**
     * Retourne l'espace libre sur une DataPage d'indice id
     * 
     * @param id l'indice de la DataPage dont on veut l'espace libre
     * @return l'espace libre de la DataPage d'indice id
     */
    public int getFreeSpace(int id) {
        mBuffer.position(Integer.BYTES + id * Integer.BYTES * 3 + 2 * Integer.BYTES);

        return mBuffer.getInt();
    }

    /**
     * Modifie l'espace libre de la DataPage d'indice id
     * 
     * @param id     l'indice de la DataPage dont on veut modifier l'espace libre
     * @param taille la nouvelle taille de l'espace libre
     */
    public void setFreeSpace(int id, int taille) {
        mDirty = true;

        mBuffer.position(Integer.BYTES + id * Integer.BYTES * 3 + 2 * Integer.BYTES);
        mBuffer.putInt(taille);
    }

    /**
     * Getter de la place restante dans la HeaderPage
     * 
     * @return la place restante dans la HeaderPage
     */
    public int getSpaceLeftInHeader() {
        return DBParams.pageSize - Integer.BYTES - 3 * getDataPageCount() * Integer.BYTES;
    }

    /**
     * Libère la HeaderPage
     */
    public void free() {
        BufferManagerCopy.getSingleton().freePage(mPageId, mDirty);
    }

    /**
     * Affiche le contenu de la HeaderPage sous forme de chaine de caractères
     * 
     * @return le contenu de la HeaderPage sous forme de String
     */
    @Override
    public String toString() {
        StringBuilder build = new StringBuilder();

        build.append("Nombre de DataPages: " + getDataPageCount() + "\n");

        for (int i = 0; i < getDataPageCount(); i++) {
            build.append(getDataPageId(i) + ": " + getFreeSpace(i) + "\n");
        }

        return build.toString();
    }

    public PageId getPid() {
        return mPageId;
    }

    public int getDataPageIdIndex(PageId pid) {
        for (int i = 0; i < getDataPageCount(); i++) {
            if (getDataPageId(i).equals(pid)) {
                return i;
            }
        }

        return -1;
    }
}
