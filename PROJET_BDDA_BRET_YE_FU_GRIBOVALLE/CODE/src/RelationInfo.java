import java.io.Serializable;

/**
 * Classe qui donne les informations d'une relation
 * 
 * @author GRIBOVALLE Mathieu, BRET David, YE Frederic,FU SuTanqQin
 * @version 1.0
 * @date 2022-11-03
 */
public class RelationInfo implements Serializable {
    /**
     * Le nom de la relation
     */
    private String nomRelation;

    /**
     * Le nombre de colonnes de la relation
     */
    private int nbColonnes;

    /**
     * Les colonnes de la relation
     */
    private ColInfo[] colonnes;

    /**
     * Le PageId de la HeaderPage de la relation
     */
    private PageId headerPageId;

    /**
     * Constructeur de RelationInfo
     * 
     * @param nom          le nom de la relation
     * @param nbColonnes   le nombre de colonnes de la relation
     * @param colonnes     les colonnes de la relation
     * @param headerPageId le PageId de la HeaderPage de la relation
     */
    public RelationInfo(String nom, int nbColonnes, ColInfo[] colonnes, PageId headerPageId) {
        this.nomRelation = nom;
        this.nbColonnes = nbColonnes;
        this.colonnes = colonnes;
        this.headerPageId = headerPageId;
    }

    /**
     * Getter du nom de la relation
     * 
     * @return le nom de la relation
     */
    public String getNomRelation() {
        return nomRelation;
    }

    /**
     * Getter des informations des colonnes de la relation
     * 
     * @return les informations des colonnes de la relation
     */
    public ColInfo[] getColonnes() {
        return colonnes;
    }

    /**
     * Getter du nombre de colonnes de la relation
     * 
     * @return le nombre de colonnes de la relation
     */
    public int getNbColonnes() {
        return nbColonnes;
    }

    /**
     * Getter du PageId de la HeaderPage de la relation
     * 
     * @return le PageId de la HeaderPage de la relation
     */
    public PageId getHeaderPageId() {
        return headerPageId;
    }
}