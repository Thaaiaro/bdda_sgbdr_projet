/**
 * Classe permettant la gestion du SGBD
 * 
 * @author GRIBOVALLE Mathieu, BRET David, YE Frederic, FU SuTanqQin
 * @version 1.0
 * @date 2022-11-24
 */
public class DBManager {
    /**
     * Crée une unique instance du DBManager
     */
    private static DBManager g_instance = new DBManager();

    /**
     * Initialise le SGBD
     * 
     * @throws Exception
     */
    public void init() throws Exception {
        Catalog.getSingleton().init();
    }

    /**
     * Ferme le SGBD
     */
    public void finish() {
        Catalog.getSingleton().finish();
        BufferManagerCopy.getSingleton().flushAll();
    }

    /**
     * Action du SGBD en fonction de la commande rentrée par l'utilisateur
     * 
     * @param commande commande à traiter
     */
    public void processCommand(String commande) throws Exception {
        String[] mots = commande.split(" ");

        switch (mots[0]) {

            case "CREATE":
                if (!mots[1].equals("TABLE"))
                    System.out.println("Commande invalide (" + commande + ")");
                else {
                    System.out.println("created");
                    // DBManager.getSingleton().processCommand(command);
                }
                break;

            case "DROPDB":
                DropDBCommand.execute();
                System.out.println("DB cleared");
                break;

            case "INSERT":
                System.out.println("inserted");
                // DBManager.getSingleton().processCommand(command);
                break;

            case "SELECT":
                System.out.println("selected");
                // DBManager.getSingleton().processCommand(command);
                break;

            default:
                System.out.println("Commande invalide (" + commande + ")");
                break;
        }

    }

    /**
     * Création de l'instance unique du DBManager
     * 
     * @return l'instance de DBManager
     */
    public static DBManager getSingleton() {
        return g_instance;
    }
}
