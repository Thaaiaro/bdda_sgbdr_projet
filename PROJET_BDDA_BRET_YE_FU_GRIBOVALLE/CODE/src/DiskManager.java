import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;

/**
 * Classe permettant la gestion du disque
 * 
 * @author GRIBOVALLE Mathieu, BRET David, YE Frederic, FU SuTanqQin
 * @version 1.0
 * @date 2022-09-15
 */
public class DiskManager {

    /**
     * Crée l'unique instance du DiskManager
     */
    private static DiskManager g_instance = new DiskManager();

    /**
     * Nombre de pages allouées
     */
    private int currentCountAllocPages;

    /**
     * Liste des pages allouées
     */
    private HashMap<PageId, Boolean> listePages = new HashMap<>();

    /**
     * Vide le fichier de la Base de données
     */
    public static void clearDB(File dirPath) {
        File filesList[] = dirPath.listFiles();

        for (File file : filesList) {
            if (file.isFile()) {
                file.delete();
            } else {
                clearDB(file);
            }
        }
    }

    /**
     * Alloue une page
     * 
     * @return le PageId de la page allouée
     */
    public PageId allocPage() {
        int fileX = 0;
        int pageX = 0;
        boolean espaceTrouve = false;

        PageId nouvPage = null;

        for (Map.Entry<PageId, Boolean> mapEntry : listePages.entrySet()) {
            if (mapEntry.getValue().equals(false)) {
                nouvPage = new PageId(fileX, pageX);
                listePages.put(nouvPage, true);
                espaceTrouve = true;
                return nouvPage;
            }
            pageX += 1;
            if (pageX == 4) {
                fileX += 1;
                pageX = 0;
            }
        }

        if (!espaceTrouve) {

            if (pageX == 0) {
                try {
                    RandomAccessFile rf = new RandomAccessFile(DBParams.DBPath + "F" + fileX + ".bdda", "rw");
                    nouvPage = new PageId(fileX, pageX);
                    listePages.put(nouvPage, true);
                    rf.close();
                } catch (IOException e) {
                    System.err.println(e.getMessage());
                    System.exit(1);
                }

            } else {
                nouvPage = new PageId(fileX, pageX);
                listePages.put(nouvPage, true);
            }
        }
        return nouvPage;
    }

    /**
     * Remplit le buffer buff avec le contenu disque de la page identifiée par pid
     * 
     * @param pid  PageId de la page
     * @param buff buffer
     */
    public void readPage(PageId pid, ByteBuffer buff) {

        try {
            RandomAccessFile rf = new RandomAccessFile(DBParams.DBPath + "F" + pid.getFileIdx() + ".bdda", "r");
            rf.seek(DBParams.pageSize * pid.getPageIdx());
            rf.read(buff.array(), 0, DBParams.pageSize);
            rf.close();
        } catch (FileNotFoundException e) {
            System.err.println(e.getMessage());
            System.exit(1);
        } catch (IOException e) {
            System.err.println(e.getMessage());
            System.exit(1);
        }

    }

    /**
     * Ecrit le contenu du buffer dans le fichier à la position indiquée par le pid
     * 
     * @param pid  PageId de la page à écrire
     * @param buff buffer
     */
    public void writePage(PageId pid, ByteBuffer buff) {

        try {
            RandomAccessFile rf = new RandomAccessFile(DBParams.DBPath + "F" + pid.getFileIdx() + ".bdda", "rw");
            rf.seek(DBParams.pageSize * pid.getPageIdx());
            rf.write(buff.array(), 0, DBParams.pageSize);
            rf.close();
        } catch (FileNotFoundException e) {
            System.err.println(e.getMessage());
            System.exit(1);
        } catch (IOException e) {
            System.err.println(e.getMessage());
            System.exit(1);
        }

    }

    /**
     * désalloue une page
     * 
     * @param pid PageId de la page à désallouer
     */
    public void deallocPage(PageId pid) {
        // Cette méthode doit désallouer une page
        listePages.put(pid, false);
    }

    /**
     * Donne le nombre de page allouée
     * 
     * @return le nombre de page allouée
     */
    public int getCurrentCountAllocPages() {
        return currentCountAllocPages;
    }

    /**
     * Création de l'unique instance du DiskManager
     * 
     * @return l'instance de DiskManager
     */
    public static DiskManager getSingleton() {
        return g_instance;
    }
}
