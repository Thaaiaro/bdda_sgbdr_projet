import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;

//ATTENTION A AVOIR AUTANT DE FREEPAGE QUE DE GETPAGE

public class BufferManager {

    private static BufferManager g_instance = new BufferManager();
    private Map<PageId, Frame> mFramesByPageId = new HashMap<PageId, Frame>();

    public ByteBuffer getPage(PageId pid) {
        if (!mFramesByPageId.containsKey(pid)) {
            Frame f = new Frame();
            f.setPid(pid);
            mFramesByPageId.put(pid, f);
        }

        return mFramesByPageId.get(pid).getBuff();
    }

    public void freePage(PageId pid, boolean valDirty) {
    }

    public void flushAll() {
    }

    public static BufferManager getSingleton() {
        return g_instance;
    }
}