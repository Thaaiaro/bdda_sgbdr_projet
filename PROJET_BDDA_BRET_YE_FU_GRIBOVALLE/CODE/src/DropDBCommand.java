import java.io.File;
import java.io.IOException;

public class DropDBCommand {

    public static void execute() throws IOException {
        File file = new File(DBParams.DBPath);
        DiskManager.clearDB(file);
    }
}
