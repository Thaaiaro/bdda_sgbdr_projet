import java.nio.ByteBuffer;

public class RecordTest {
    public static void main(String[] args) {
        DBParams.pageSize = 4096;
        ColInfo col1 = new ColInfo("Age", "INTEGER");
        ColInfo col2 = new ColInfo("taille", "REAL");
        ColInfo col3 = new ColInfo("prénom", "VARCHAR");
        ColInfo[] tabCol = { col1, col2, col3 };

        PageId pid = new PageId(0, 0);

        RelationInfo Etudiant = new RelationInfo("Etudiant", 3, tabCol, pid);

        Catalog cat = Catalog.getSingleton();
        cat.addRelationInfo(Etudiant);

        Record rec = new Record(Etudiant);
        // NECESSITE RECORD VALUES EN PUBLIC
        rec.values.set(0, "21");
        rec.values.set(1, "1.82");
        rec.values.set(2, "Benoit");

        ByteBuffer buff = ByteBuffer.allocate(DBParams.pageSize * 4);
        rec.writeToBuffer(buff, 0);
        System.out.println(rec);

        Record rec2 = new Record(Etudiant);
        rec2.values.set(0, "20");
        rec2.values.set(1, "1.72");
        rec2.values.set(2, "Mathieu");
        rec2.readFromBuffer(buff, 0);
        System.out.println(rec2);

    }

    // Tu crées un Catalogue, avec une relation Etudiant (age, taille, prenom sur
    // char16)
    // Tu crées un Record, sur la relation Etudiant
    // Tu lui donnes une valeur (21, 1.82, "Benoit") (1)
    // Tu crées un ByteBuffer
    // Tu appelles writeToBuffer()

    // Tu crées un nouveau Record (toujours sur Etudiant)
    // Tu lui files le buffer a lire
    // Tu regardes si les values sont bien egales a (1)
}
