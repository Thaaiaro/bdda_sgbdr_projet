import java.nio.ByteBuffer;

/**
 * Classe itératrice pour les Record
 * 
 * @author GRIBOVALLE Mathieu, BRET David, YE Frederic,FU SuTanqQin
 * @version 1.0
 * @date 2022-11-24
 */
public class RecordIterator {

    /**
     * Buffer/page
     */
    ByteBuffer buff;

    /**
     * PageId de la page
     */
    PageId pid;

    /**
     * Relation de la page
     */
    RelationInfo relInfo;

    /**
     * Constructeur de l'itérateur de Record
     */
    public RecordIterator(RelationInfo relInfo, PageId pid) {
        buff = BufferManagerCopy.getSingleton().getPage(pid);
        buff.position(0);

        this.relInfo = relInfo;
        this.pid = pid;
    }

    /**
     * Donne le record suivant
     * 
     * @return le record suivant
     */
    public Record getNextRecord() {
        for (int i = 0; i < relInfo.getNbColonnes(); i++) {
            if (relInfo.getColonnes()[i].getType() == "INTEGER")
                buff.getInt();
            else if (relInfo.getColonnes()[i].getType() == "REAL")
                buff.getFloat();
        }
        return new Record(relInfo);// A FINIR
    }

    /**
     * Permet de fermer l'itérateur de Record
     */
    public void close() {
        BufferManagerCopy.getSingleton().freePage(pid, false);
    }

    /**
     * Remet l'itérateur à la position de départ
     */
    public void reset() {
        buff.position(0);
    }
}
