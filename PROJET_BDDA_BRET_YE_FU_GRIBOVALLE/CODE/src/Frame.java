import java.nio.ByteBuffer;

/**
 * Classe permettant de structurer et d'accéder aux éléments de notre Frame dans
 * le Buffer
 * 
 * @author GRIBOVALLE Mathieu, BRET David, YE Frederic, FU SuTanqQin
 * @version 1.2
 * @date 2022-10-21
 */
public class Frame {

    /**
     * Buffer de la Frame
     */
    private ByteBuffer buff;

    /**
     * PageId de la page chargée dans la Frame
     */
    private PageId pid;

    /**
     * Nombre d'utilisateur ayant chargé le contenu de la Frame
     */
    private int pin_count;

    /**
     * FlagDirty de la frame, false si la page n'a pas été modifié par l'accesseur,
     * true sinon
     */
    private boolean flag_dirty;

    /**
     * Constructeur d'une frame sans page allouée
     */
    public Frame() {
        this.pin_count = 0;
        this.flag_dirty = false;
        this.buff = ByteBuffer.allocate(DBParams.pageSize);
    }

    /**
     * Getter du PageId de la page chargée dans la Frame
     * 
     * @return le PageId de la page chargée dans la Frame
     */
    public PageId getPid() {
        return pid;
    }

    /**
     * Getter du buffer de la frame
     * 
     * @return le buffer de la frame
     */
    public ByteBuffer getBuff() {
        return buff;
    }

    /**
     * Getter du nombre d'utilisateur ayant chargé le contenu de la Frame
     * 
     * @return le nombre d'utilisateur ayant chargé le contenu de la Frame
     */
    public int getPinCount() {
        return pin_count;
    }

    /**
     * Getter du FlagDirty de la frame
     * 
     * @return le FlagDirty
     */
    public boolean getFlagDirty() {
        return flag_dirty;
    }

    /**
     * Setter du nombre d'utilisateur ayant chargé le contenu de la Frame
     * 
     * @param pin_count le nouveau nombre d'utilisateur ayant chargé le contenu de
     *                  la Frame
     */
    public void setPinCount(int pin_count) {
        this.pin_count = pin_count;
    }

    /**
     * Change le page chargée dans la frame
     * 
     * @param pid le PageId de la nouvelle page chargée
     */
    public void setPid(PageId pid) {
        this.pid = pid;
        buff.position(0);
    }

    /**
     * Setter du FlagDirty de la frame
     * 
     * @param flag_dirty le FlagDirty
     */
    public void setFlagDirty(boolean flag_dirty) {
        this.flag_dirty = flag_dirty;
    }

    /**
     * Setter du buffer de la frame
     * 
     * @param buff le buffer
     */
    public void setBuff(ByteBuffer buff) {
        this.buff = buff;
    }

    /**
     * Affiche le contenu de la Frame sous la forme d'une chaine de caractère
     * 
     * @return le contenu de la Frame sous forme de String
     */
    @Override
    public String toString() {
        return "PageId: " + pid.toString() + "\nPinCount: " + pin_count + "\nFlagDirty: " + flag_dirty;
    }
}
