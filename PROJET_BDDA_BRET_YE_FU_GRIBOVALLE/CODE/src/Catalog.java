import java.util.List;
import java.util.ArrayList;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.NotSerializableException;
import java.io.ObjectOutputStream;
import java.io.ObjectStreamException;
import java.io.Serializable;
import java.io.ObjectInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

/**
 * Classe de l'unique instance du Catalog de notre base de données, Répertorie
 * toutes les relations dans la base de données
 * 
 * @author GRIBOVALLE Mathieu, BRET David, YE Frederic,FU SuTanqQin
 * @version 1.0
 * @date 2022-11-03
 */
public class Catalog implements Serializable {

    /**
     * Crée l'unique instance de Catalog
     */
    private static Catalog g_instance = new Catalog();

    /**
     * liste des relations dans le Catalog
     */
    private List<RelationInfo> relations = new ArrayList<>();

    // Lit Catalog.sv et savegarde le content dans list<RelationInfo> relations
    public void init() throws Exception {
        try {
            File f = new File(DBParams.DBPath + "/Catalog.sv");
            if (f.exists()) {
                // System.out.println("Le fichier existe");
                FileInputStream fis = new FileInputStream("./PROJET_BDDA_BRET_YE_FU_GRIBOVALLE/DB/Catalog.sv");
                ObjectInputStream ois = new ObjectInputStream(fis);

                relations = (List<RelationInfo>) ois.readObject();
                ois.close();

                // for(RelationInfo r : relations)
                // System.out.println(r.getNomRelation()+" ");
            }

        } catch (FileNotFoundException e) {
            System.out.println(e);
            System.out.println("Erreur init : Fichier non trouvé.");
        } catch (ObjectStreamException e) {
            System.out.println(e);
            System.out.println("Erreur");
        } catch (ClassNotFoundException e) {
            System.out.println(e);
            System.out.println("Erreur init : classe non trouvé.");
        } catch (IOException e) {
            System.out.println(e);
            System.out.println("Erreur init : input invalide.");
        }
    }

    // Sauvegarde list<RelationsInfo> relations dans Catalog.sv
    public void finish() {
        try {
            File f = new File(DBParams.DBPath + "/Catalog.sv");
            if (!f.exists()) {
                f.createNewFile();
                // System.out.println("Fichier crée avec succes");
            }
            FileOutputStream fos = new FileOutputStream(f);
            ObjectOutputStream oos = new ObjectOutputStream(fos);

            oos.writeObject(relations);
            oos.flush();
            oos.close();

        } catch (FileNotFoundException e) {
            System.out.println(e);
            System.out.println("Erreur finish : fichier non trouvé.");
        } catch (NotSerializableException nse) {
            System.out.println("Exception non sérialisable");
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Erreur finish : Path invalide.");
        }
    }

    public List<RelationInfo> getRelations() {
        return relations;
    }

    /**
     * Ajout d'une relation dans le Catalog
     * 
     * @param rel relation à ajouter
     */
    public void addRelationInfo(RelationInfo rel) {
        relations.add(rel);
        // System.out.println("relation ajouté : " + rel.getNomRelation());
    }

    /**
     * Retourne la relation dans Catalog de nom nomRel
     * 
     * @param nomRel nom de la relation à trouver
     * @return la relation nommé nomRel
     * @throws Exception si la relation n'est pas trouvée
     */
    public RelationInfo getRelationInfo(String nomRel) throws Exception {
        for (RelationInfo rel : relations) {
            if (rel.getNomRelation().equals(nomRel))
                System.out.println("Relation retourné :" + rel.getNomRelation());
            return rel;
        }
        throw new Exception("Relation non trouvée");
    }

    /**
     * Création de l'instance unique de Catalog
     * 
     * @return l'instance de Catalog
     */
    public static Catalog getSingleton() {
        return g_instance;
    }

    /**
     * Affiche le Catalog
     * 
     * @return le catalog sous forme de String
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();

        for (RelationInfo tmp : relations) {
            sb.append(tmp.getNomRelation() + ", ");
        }
        return sb.toString();
    }
}
