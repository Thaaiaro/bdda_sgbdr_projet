/**
 * Classe qui permet de situer un Record par son identifiant
 * 
 * @author GRIBOVALLE Mathieu, BRET David, YE Frederic, FU SuTanqQin
 * @version 1.0
 * @date 2022-11-17
 */
public class RecordId {
    /**
     * Le PageId de la page contenant le record
     */
    private PageId pid;

    /**
     * La position du record dans la page
     */
    private int slotIdx;

    /**
     * Constructeur de RecordId
     * 
     * @param pid     le PageId de la page contenant le record
     * @param slotIdx la position du record dans la page
     */
    public RecordId(PageId pid, int slotIdx) {
        this.pid = pid;
        this.slotIdx = slotIdx;
    }

    /**
     * Getter du PageId de la page contenant le record
     * 
     * @return le PageId de la page contenant le record
     */
    public PageId getPid() {
        return pid;
    }

    /**
     * Getter de la position du record dans la page
     * 
     * @return la position du record dans la page
     */
    public int getSlotIdx() {
        return slotIdx;
    }

    /**
     * Affichage du RecordId
     * 
     * @return le RecordId sous forme de chaine de caractère
     */
    @Override
    public String toString() {
        return "RecordId: (" + pid.toString() + ", " + slotIdx + ")";
    }
}