import java.util.List;

/**
 * Classe sur la commande CreateTable
 * 
 * @author GRIBOVALLE Mathieu, BRET David, YE Frederic, FU SuTanqQin
 * @version 1.0
 * @date 2022-11-24
 */
public class CreateTableCommand {

    /**
     * Nom de la relation
     */
    private String nomRelation;

    /**
     * Nombre de colonnes de la relation
     */
    private int nbColonnes;

    /**
     * Noms des colonnes de la relation
     */
    private List<String> nomsColonnes;

    /**
     * Types des colonnes de la relation
     */
    private List<String> typesColonnes;

    /**
     * Constructeur de la commande CreateTableCommand
     */
    public CreateTableCommand() {

    }

    /**
     * Exécution de la commande CreateTable
     */
    public void execute() {
        PageId pidHPage = FileManager.getSingleton().createNewHeaderPage();

        ColInfo[] colonnes = new ColInfo[nbColonnes];
        for (int i = 0; i < nbColonnes; i++) {
            colonnes[i] = new ColInfo(nomsColonnes.get(i), typesColonnes.get(i));
        }

        RelationInfo relInfo = new RelationInfo(nomRelation, nbColonnes, colonnes, pidHPage);

        System.out.println(relInfo);
    }
}
