import java.util.ArrayList;
import java.nio.CharBuffer;
import java.nio.ByteBuffer;

/**
 * Classe qui définit une instance d'une relation
 * 
 * @author GRIBOVALLE Mathieu, BRET David, YE Frederic, FU SuTanqQin
 * @version 1.0
 * @date 2022-11-10
 */
public class Record {

    /**
     * Les informations sur la relation dont le record est une instance
     */
    private RelationInfo relInfo;

    /**
     * Les valeurs du Record
     */
    public ArrayList<String> values;

    /**
     * Constructeur d'un nouveau Record
     * 
     * @param rel la relation dont le record est une instance
     */
    public Record(RelationInfo rel) {
        relInfo = rel;
        values = new ArrayList<>();

        for (int i = 0; i < relInfo.getNbColonnes(); i++) {
            values.add("");
        }
    }

    /**
     * Permet l'écriture d'un Record vers un buffer/une page
     * 
     * @param buff le buffer (la page) dans lequel on veut écrire le Record
     * @param pos  la position où l'on veut écrire le Record
     */
    public void writeToBuffer(ByteBuffer buff, int pos) {
        buff.position(pos);
        buff.putInt(Integer.BYTES * (relInfo.getNbColonnes() + 1));

        for (int i = 0; i < values.size(); i++) {
            if (relInfo.getColonnes()[i].getType() == "INTEGER" || relInfo.getColonnes()[i].getType() == "REAL")
                buff.putInt(Integer.BYTES);
            else {
                buff.putInt(values.get(i).length() * Character.BYTES);
            }
        }

        buff.position(pos + Integer.BYTES * (relInfo.getNbColonnes() + 1));
        for (int i = 0; i < values.size(); i++) {
            if (relInfo.getColonnes()[i].getType() == "INTEGER")
                buff.putInt(Integer.parseInt(values.get(i)));
            else if (relInfo.getColonnes()[i].getType() == "REAL")
                buff.putFloat(Float.parseFloat(values.get(i)));
            else {
                CharBuffer cbuf = buff.asCharBuffer();
                cbuf.put(values.get(i));
            }
        }
    }

    /**
     * Permet la lecture d'un Record depuis un buffer/une page
     * 
     * @param buff le buffer (la page) qu'on veut lire
     * @param pos  la position à partir de laquelle on veut lire
     */
    public void readFromBuffer(ByteBuffer buff, int pos) {

        buff.position(pos + Integer.BYTES);
        ArrayList<Integer> offsetValues = new ArrayList<>();

        for (int i = 0; i < relInfo.getNbColonnes(); i++) {
            offsetValues.add(buff.getInt());
        }

        buff.position(pos + Integer.BYTES * (relInfo.getNbColonnes() + 1));

        for (int i = 0; i < relInfo.getNbColonnes(); i++) {
            if (relInfo.getColonnes()[i].getType() == "INTEGER")
                values.set(i, buff.getInt() + "");
            else if (relInfo.getColonnes()[i].getType() == "REAL")
                values.set(i, buff.getFloat() + "");
            else {
                int nbChar = offsetValues.get(i) / Character.BYTES;
                values.set(i, "");
                for (int j = 0; j < nbChar; j++) {
                    values.set(i, values.get(i) + buff.getChar());
                }
            }
        }
    }

    /**
     * Getter de la relation du Record
     * 
     * @return la RelationInfo du Record
     */
    public RelationInfo getRelInfo() {
        return relInfo;
    }

    /**
     * Retourne la taille d'un Record
     * 
     * @return la taille d'un Record
     */
    public int getSize() {
        int taille = 0;
        for (int i = 0; i < relInfo.getNbColonnes(); i++) {
            if (relInfo.getColonnes()[i].getType() == "INTEGER")
                taille += Integer.BYTES;
            else if (relInfo.getColonnes()[i].getType() == "REAL")
                taille += Float.BYTES;
            else
                taille += values.get(i).length() * Character.BYTES;
        }
        return taille;
    }

    /**
     * Affiche le Record sous la forme d'une chaine de caractère
     * 
     * @return le Record sous forme de String
     */
    @Override
    public String toString() {
        String res = "Record:\n";
        for (int i = 0; i < values.size(); i++) {
            res += values.get(i) + "\n";
        }
        return res;
    }
}
