import java.util.Scanner;

public class App {
    public static void main(String[] args) throws Exception {
        Scanner sc = new Scanner(System.in);
        DBManager.getSingleton().init();

        System.out.println("Bienvenue !");
        System.out.println("(Commande acceptées : CREATE TABLE, DROPDB, INSERT, SELECT, EXIT)");

        while (true) {
            System.out.println("\nvotre commande :");
            String commande = sc.nextLine();
            if (commande.equals("EXIT")) {
                System.out.println("au revoir!");
                DBManager.getSingleton().finish();
                break;
            }
            DBManager.getSingleton().processCommand(commande);
        }
        sc.close();
        System.exit(0);
    }
}
