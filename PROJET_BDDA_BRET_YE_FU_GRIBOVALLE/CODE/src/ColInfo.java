import java.io.Serializable;

/**
 * Classe qui donne les informations des colonnes
 * 
 * @author GRIBOVALLE Mathieu, BRET David, YE Frederic, FU SuTanqQin
 * @version 1.0
 * @date 2022-11-03
 */
public class ColInfo implements Serializable {

    /**
     * Nom de la colonne
     */
    private String nom;

    /**
     * Type de la colonne
     */
    private String type;

    /**
     * Constructeur des informations d'une colonne
     * 
     * @param nom  nom de la colonne
     * @param type type de la colonne
     */
    public ColInfo(String nom, String type) {
        this.nom = nom;
        this.type = type;
    }

    /**
     * Getter du nom de la colonne
     * 
     * @return nom de la colonne
     */
    public String getNom() {
        return nom;
    }

    /**
     * Getter du type de la colonne
     * 
     * @return type de la colonne
     */
    public String getType() {
        return type;
    }

    /**
     * Affichage des informations de la colonne
     * 
     * @return informations de la colonne sous forme de String
     */
    @Override
    public String toString() {
        return "ColInfo: (" + nom + ", " + type + ")\n";
    }
}
