import java.nio.ByteBuffer;

public class BufferManagerTest {
    public static void main(String[] args) throws Exception {
        DBParams.DBPath = "./DB/";
        DBParams.pageSize = 4096;
        DBParams.maxPagesPerFile = 4;
        DBParams.frameCount = 2;

        // TEST DE LA CLASSE BUFFERMANAGER
        DiskManager dm = DiskManager.getSingleton();
        BufferManagerCopy bm = BufferManagerCopy.getSingleton();

        // dm.clearDB();

        PageId pageId1 = dm.allocPage();
        PageId pageId2 = dm.allocPage();
        PageId pageId3 = dm.allocPage();
        // PageId pageId4 = dm.allocPage();

        ByteBuffer b1 = bm.getPage(pageId1);
        b1.putInt(1);
        ByteBuffer b2 = bm.getPage(pageId2);
        b2.putInt(2);
        bm.freePage(pageId2, true);
        ByteBuffer b3 = bm.getPage(pageId3);
        b3.putInt(3);
        System.out.println(b3.getInt());
        bm.freePage(pageId1, true);
        bm.freePage(pageId3, true);
        ByteBuffer b4 = bm.getPage(pageId1);
        System.out.println(b4.getInt());
        ByteBuffer b5 = bm.getPage(pageId2);
        System.out.println(b5.getInt());

        // bm.getPage(pageId1);

        ByteBuffer bf = ByteBuffer.allocate(DBParams.pageSize);
        bf.putChar('t');
        dm.writePage(pageId1, bf);

        ByteBuffer bf2 = ByteBuffer.allocate(DBParams.pageSize);
        dm.readPage(pageId1, bf2);

        bf.position(0);

        assert (bf2.getChar() == bf.getChar());

        bm.freePage(pageId1, true);
        bm.freePage(pageId2, false);
        bm.getPage(pageId3);
        bm.getPage(pageId2);

        // System.out.println(BufferManager.getSingleton().listeFrames[0]);
        // bm.getPage(pageId4); // =>CENSE CRASH

    }
}
